<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?= APP_URL ?>">Демонстрационный проект</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <?php if (!empty($user) && !empty($title)): ?>
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= $route ?>"><?= $title ?></a>
      </li>
    </ul>
      <?php endif; ?>
    <ul class="navbar-nav ml-auto">
        <?php if (!empty($user)): ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle btn btn-outline-success my-2 my-sm-0" href="#" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Привет, <?= $user->getNickname() ?></a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/users/logout">Выйти</a>
                </div>
            </li>
        <?php else: ?>
            <li class="nav-item"><a class="nav-link" href="/users/login">Войти</a></li>
            <li class="nav-item"><a class="nav-link" href="/users/register">Зарегистрироваться</a></li>
        <?php endif; ?>
    </ul>
  </div>
</nav>
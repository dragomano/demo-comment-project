<?php if ($paginator): ?>
    <div class="row justify-content-center">
        <div class="text-center">
            <nav aria-label="...">
                <ul class="pagination">

                    <?php if ($paginator->getPrevUrl()): ?>
                        <li class="page-item">
                            <a class="page-link" href="<?php echo $paginator->getPrevUrl(); ?>">&laquo; Назад</a>
                        </li>
                    <?php else: ?>
                        <li class="page-item disabled">
                            <span class="page-link">&laquo; Назад</span>
                        </li>
                    <?php endif; ?>

                    <?php foreach ($paginator->getPages() as $page): ?>
                        <?php if ($page['url']): ?>
                            <li class="page-item<?php echo $page['isCurrent'] ? ' active aria-current="page' : ''; ?>">
                                <?php if ($page['isCurrent']): ?>
                                    <span class="page-link">
                                <?= $page['num'] ?>
                                <span class="sr-only">(current)</span>
                            </span>
                                <?php else: ?>
                                    <a class="page-link" href="<?php echo $page['url']; ?>"><?php echo $page['num']; ?></a>
                                <?php endif; ?>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    <?php if ($paginator->getNextUrl()): ?>
                        <li class="page-item">
                            <a class="page-link" href="<?php echo $paginator->getNextUrl(); ?>">Далее &raquo;</a>
                        </li>
                    <?php else: ?>
                        <li class="page-item disabled">
                            <span class="page-link">Далее &raquo;</span>
                        </li>
                    <?php endif; ?>

                </ul>
            </nav>
        </div>
    </div>
<?php endif; ?>
<?php
/**
 * @var App\Models\Comments\Comment $comment
 */
?>
    <div class="row">
        <div class="col-md-12">
            <div class="pt-3 pt-md-5">
                <div class="p-3">
                    <h2 class="display-5 text-center">Комментарии</h2>
                </div>
                <div class="bg-dark shadow-sm mx-auto" style="border-radius: 21px 21px 0 0;">
                    <?php if ($comments): ?>
                        <table class="table table-striped table-hover table-dark">
                            <thead>
                            <tr class="text-center">
                                <th scope="col"><a href="/?sort=date">Дата</a></th>
                                <th scope="col">Картинка</th>
                                <th scope="col"><a href="/?sort=name">Автор</a></th>
                                <?php if ($user && $user->isAdmin()): ?>
                                    <th scope="col"><a href="/?sort=email">Email</a></th>
                                <?php endif; ?>
                                <th scope="col">Текст</th>
                                <?php if ($user && $user->isAdmin()): ?>
                                    <th scope="col">Статус</th>
                                    <th scope="col">Действия</th>
                                <?php endif; ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($comments as $comment): ?>
                                <tr>
                                    <td class="text-center"><?= $comment->getCreatedAt() ?></td>
                                    <td class="text-center">
                                        <?php if ($comment->getImage()): ?>
                                            <a href="/uploads/<?= $comment->getImage() ?>"><img src="/uploads/<?= $comment->getImage() ?>" class="mr-3" width="64" alt="Картинка"></a>
                                        <?php else: ?>
                                            Нет
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center"><?= $comment->getAuthor(); ?></td>
                                    <?php if ($user && $user->isAdmin()): ?>
                                        <td class="text-center"><?= $comment->getEmail(); ?></td>
                                    <?php endif; ?>
                                    <td>
                                        <?= $comment->getParsedContent() ?>
                                        <?php if ($comment->getUpdatedAt()): ?>
                                            <span class="badge badge-pill badge-warning">Изменено администратором</span>
                                        <?php endif; ?>
                                    </td>
                                    <?php if ($user && $user->isAdmin()): ?>
                                        <td class="text-center">
                                            <?php if ($comment->getStatus()): ?>
                                                <a href="/comments/<?= $comment->getId() ?>/toggle" class="badge badge-success">Опубликован</a>
                                            <?php else: ?>
                                                <a href="/comments/<?= $comment->getId() ?>/toggle" class="badge badge-danger">Отклонен</a>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                            <a href="/comments/<?= $comment->getId() ?>/edit" class="badge badge-secondary">Изменить</a>
                                            <a href="/comments/<?= $comment->getId() ?>/delete" class="badge badge-secondary">Удалить</a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                        <?php include __DIR__ . '/../partials/pagination.php'; ?>

                    <?php else: ?>
                        <div class="alert alert-info" role="alert">
                            Комментариев пока нет
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php include __DIR__ . '/../comments/add.php'; ?>
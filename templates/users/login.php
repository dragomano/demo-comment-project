    <div class="row">
        <div class="col-md-12">
            <div class="mr-md-3 pt-3 px-3 pt-md-5 px-md-5">
                <div class="p-3">
                    <h2 class="display-5 text-center">Вход</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php if (!empty($error)): ?>
                            <div class="alert alert-danger"><?= $error ?></div>
                        <?php endif; ?>

                        <form class="login-register-form text-white bg-dark" action="/users/login" method="post">
                            <div class="form-group">
                                <label for="login">Логин</label>
                                <input type="text" id="login" name="login" class="form-control" placeholder="Логин" value="<?= $_POST['login'] ?? '' ?>" required autofocus>
                            </div>
                            <div class="form-group">
                                <label for="password">Пароль</label>
                                <input type="password" id="password" name="password" class="form-control" placeholder="Пароль" value="<?= $_POST['password'] ?? '' ?>" required>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

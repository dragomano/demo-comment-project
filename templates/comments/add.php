    <div class="row mt-3">
        <div class="col-md-12">
            <div id="results"></div>
            <div class="card text-white bg-dark shadow-sm mb-3">
                <div class="card-header">Оставить комментарий</div>
                <div class="card-body">
                    <form id="comment_form" class="needs-validation" novalidate action="/comments/add" method="post" enctype="multipart/form-data">
                        <?php if (empty($user)): ?>
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="name">Имя</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Вася Пупкин" required>
                                    <div class="valid-feedback">
                                        Приятно познакомиться!
                                    </div>
                                    <div class="invalid-feedback">
                                        Человек без имени не человек, ему не хватает самого существенного.
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="email">Email</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="emailGroupPrepend">@</span>
                                        </div>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="vasja@pupkin.ru" aria-describedby="emailGroupPrepend" required>
                                        <div class="valid-feedback">
                                            Почта же настоящая?
                                        </div>
                                        <div class="invalid-feedback">
                                            Пожалуйста, укажите вашу почту.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="content">Комментарий</label>
                                <textarea class="form-control" id="content" name="content" placeholder="Текст комментария" rows="3" required></textarea>
                                <div class="valid-feedback">
                                    Мы благодарны за ваше мнение!
                                </div>
                                <div class="invalid-feedback">
                                    Вам нечего сказать?
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="hidden" name="MAX_FILE_SIZE" value="<?= 1024 * 1024 ?>">
                                <input type="file" class="custom-file-input" id="image" name="image" accept=".png,.jpg,.gif">
                                <label class="custom-file-label" for="image" data-browse="Выбрать">Изображение</label>
                            </div>
                        </div>
                        <button class="btn btn-info" type="submit">Оставить комментарий</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
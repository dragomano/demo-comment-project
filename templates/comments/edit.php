<?php
/**
 * @var App\Models\Comments\Comment $comment
 */
?>

    <div class="row mt-3">
        <div class="col-md-12">
            <?php if (!empty($error)): ?>
                <div class="alert alert-danger"><?= $error ?></div>
            <?php endif; ?>
            <div id="results"></div>
            <div class="card text-white bg-dark shadow-sm mb-3">
                <div class="card-header">Редактирование комментария</div>
                <div class="card-body">
                    <form action="/comments/<?= $comment->getId() ?>/edit" method="post">
                        <div class="form-group">
                            <label for="content">Текст</label>
                            <textarea class="form-control" name="content" id="content" required><?= $comment->getContent() ?></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
/* Валидация полей формы перед отправкой */
(function() {
    'use strict';
    window.addEventListener('load', function() {
        let forms = document.getElementsByClassName('needs-validation');
        let validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

/* Ajax-отправка формы комментария */
$(document).ready(function() {
    $('form#comment_form').on('submit', (function(e) {
        let formData = new FormData(),
            name = $('#name').val(),
            email = $('#email').val(),
            content = $('#content').val();

        if (name !== undefined) {
            formData.append('name', name);
        }

        if (email !== undefined) {
            formData.append('email', email);
        }

        if (content !== undefined) {
            formData.append('content', content);
        }

        $.each($('#image')[0].files, function(i, file) {
            formData.append('image', file);
        });

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            dataType : "json",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success(data) {
                if (data['status'] === 'ok') {
                    $('#comment_form')[0].reset();
                    $('#results').html('<div class="alert alert-success fade show" role="alert">' + (data['admin'] ? 'Комментарий опубликован! Обновите страницу' : 'Комментарий отправлен на проверку!') + '</div>');
                    setTimeout(function () {
                        $("#results > div").hide('slow');
                    }, 2000);
                } else if (data['error'] !== null) {
                    $('#results').html('<div class="alert alert-danger fade show" role="alert">' + data['error'] + '</div>');
                    setTimeout(function () {
                        $("#results > div").hide('slow');
                    }, 6000);
                }
            },
            error(jqXHR, textStatus) {
                console.log(textStatus);
            }
        });
        e.preventDefault();
    }));
});
<?php

// Определим константы проекта
$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}";
$app_path = preg_replace('#[^/]+$#', '', $app_path);
define('APP_URL', $app_path);
define('ROOT', dirname(__DIR__));

require ROOT . '/vendor/autoload.php';

try {
    $route = $_GET['q'] ?? '';
    $routes = require ROOT . '/app/routes.php';

    $isRouteFound = false;
    foreach ($routes as $pattern => $controllerAndAction) {
        preg_match($pattern, $route, $matches);
        if (!empty($matches)) {
            $isRouteFound = true;
            break;
        }
    }

    if (!$isRouteFound) {
        throw new App\Exceptions\NotFoundException('Маршрут не найден!');
    }

    unset($matches[0]);

    $controllerName = $controllerAndAction[0];
    $actionName = $controllerAndAction[1];

    $controller = new $controllerName();
    $controller->$actionName(...$matches);
} catch (App\Exceptions\DbException $e) {
    $view = new App\Views\View(ROOT . '/templates/errors');
    $view->render('500', ['error' => $e->getMessage()], 500);
} catch (App\Exceptions\NotFoundException $e) {
    $view = new App\Views\View(ROOT . '/templates/errors');
    $view->render('404', ['error' => $e->getMessage()], 404);
} catch (App\Exceptions\UnauthorizedException $e) {
    $view = new App\Views\View(ROOT . '/templates/errors');
    $view->render('401', ['error' => $e->getMessage()], 401);
} catch (App\Exceptions\ForbiddenException $e) {
    $view = new App\Views\View(ROOT . '/templates/errors');
    $view->render('403', ['error' => $e->getMessage()], 403);
} catch (App\Exceptions\MissingFileException $e) {
    $view = new App\Views\View(ROOT . '/templates/errors');
    $view->render('config', ['error' => $e->getMessage()], 404);
}

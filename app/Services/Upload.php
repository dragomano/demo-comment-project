<?php

namespace App\Services;

class Upload
{
    /**
     * @var
     */
    private $file;

    /**
     * @var int
     */
    private $max_width  = 320;

    /**
     * @var int
     */
    private $max_height = 240;

    /**
     * Upload constructor.
     *
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * @return bool|string
     */
    public function upload()
    {
        $types = array('image/gif', 'image/png', 'image/jpeg');

        if (!in_array($this->file['type'], $types)) {
            return false;
        }

        $extension = pathinfo($this->file['name'], PATHINFO_EXTENSION);
        $filename  = uniqid() . '.' . $extension;

        // Изменяем изображение в соответствии с желаемыми размерами
        $this->resize($this->file['tmp_name']);

        // Перемещаем измененное изображение в папку загрузки
        move_uploaded_file($this->file['tmp_name'], 'uploads/' . $filename);

        return $filename;
    }

    /**
     * Изменение размеров изображения
     *
     * @param $filename
     */
    public function resize($filename): void
    {
        $info   = getimagesize($filename);
        $width  = $info[0];
        $height = $info[1];
        $type   = $info[2];

        switch ($type) {
            case 1:
                $img = imageCreateFromGif($filename);
                imageSaveAlpha($img, true);
                break;
            case 2:
                $img = imageCreateFromJpeg($filename);
                break;
            default:
                $img = imageCreateFromPng($filename);
                imageSaveAlpha($img, true);
                break;
        }

        $w = $this->max_width;
        $h = $this->max_height;

        if (empty($w)) {
            $w = ceil($h / ($height / $width));
        }

        if (empty($h)) {
            $h = ceil($w / ($width / $height));
        }

        $tmp = imageCreateTrueColor($w, $h);

        if ($type == 1 || $type == 3) {
            imagealphablending($tmp, true);
            imageSaveAlpha($tmp, true);
            $transparent = imagecolorallocatealpha($tmp, 0, 0, 0, 127);
            imagefill($tmp, 0, 0, $transparent);
            imagecolortransparent($tmp, $transparent);
        }

        $tw = ceil($h / ($height / $width));
        $th = ceil($w / ($width / $height));

        if ($tw < $w) {
            imageCopyResampled($tmp, $img, ceil(($w - $tw) / 2), 0, 0, 0, $tw, $h, $width, $height);
        } else {
            imageCopyResampled($tmp, $img, 0, ceil(($h - $th) / 2), 0, 0, $w, $th, $width, $height);
        }

        $img = $tmp;
        $src = $this->file['tmp_name'];

        switch ($type) {
            case 1:
                imageGif($img, $src);
                break;
            case 2:
                imageJpeg($img, $src, 100);
                break;
            case 3:
                imagePng($img, $src);
                break;
        }

        imagedestroy($img);
    }
}
<?php

namespace App\Services;

use App\Exceptions\DbException;
use App\Exceptions\MissingFileException;
use Exception;
use PDO;

class Db
{
    /** @var */
    private static $instance;

    /** @var PDO */
    private $pdo;

    /** @var int */
    private $rowCount;

    /**
     * Db constructor.
     *
     * @throws Exception
     * @throws MissingFileException
     */
    private function __construct()
    {
        $file = __DIR__ . '/../config.php';

        if (!is_file($file)) {
            throw new MissingFileException('Файл с настройками доступа к базе данных не найден!');
        }

        $dbOptions = (require $file)['db'];

        try {
            $this->pdo = new PDO(
                'mysql:host=' . $dbOptions['host'] . ';dbname=' . $dbOptions['dbname'],
                $dbOptions['user'],
                $dbOptions['password']
            );
            $this->pdo->exec('SET NAMES UTF8');
        } catch (\PDOException $e) {
            throw new DbException('Ошибка при подключении к базе данных: ' . $e->getMessage());
        }
    }

    /**
     * @return static
     * @throws Exception
     */
    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $sql
     * @param array  $params
     * @param string $className
     *
     * @return array|null
     */
    public function query(string $sql, $params = [], string $className = 'stdClass'): ?array
    {
        $sth = $this->pdo->prepare($sql);
        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
        $result = $sth->execute($params);

        if (false === $result) {
            return null;
        }

        $this->rowCount = $sth->rowCount();

        return $sth->fetchAll(PDO::FETCH_CLASS, $className);
    }

    /**
     * @return int
     */
    public function getLastInsertId(): int
    {
        return (int) $this->pdo->lastInsertId();
    }

    /**
     * @return int
     */
    public function getRowCount(): int
    {
        return (int) $this->rowCount;
    }
}
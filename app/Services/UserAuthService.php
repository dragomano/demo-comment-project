<?php

namespace App\Services;

use App\Models\Users\User;
use Exception;

class UserAuthService
{
    /**
     * @param User $user
     */
    public static function createToken(User $user): void
    {
        $token = $user->getId() . ':' . $user->getAuthToken();
        setcookie('token', $token, 0, '/', '', false, true);
    }

    /**
     * @return User|null
     * @throws Exception
     */
    public static function getUserByToken(): ?User
    {
        $token = $_COOKIE['token'] ?? '';

/*        if (empty($token)) {
            return null;
        }*/

        [$userId, $authToken] = explode(':', $token, 2);

        $user = User::getById((int) $userId);

        if ($user === null) {
            return null;
        }

        if ($user->getAuthToken() !== $authToken) {
            return null;
        }

        return $user;
    }

    /**
     * @param User $user
     */
    public static function removeToken(User $user): void
    {
        $token = $user->getId() . ':' . $user->getAuthToken();
        setcookie('token', $token, time() - 3600, '/', '', false, true);
    }
}
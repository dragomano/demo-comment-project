<?php

return [
    '~^comments/add$~'              => [App\Controllers\CommentController::class, 'add'],
    '~^comments/(\d+)/edit$~'       => [App\Controllers\CommentController::class, 'edit'],
    '~^comments/(\d+)/toggle$~'     => [App\Controllers\CommentController::class, 'toggle'],
    '~^comments/(\d+)/delete$~'     => [App\Controllers\CommentController::class, 'delete'],
    '~^users/register$~'            => [App\Controllers\UserController::class, 'register'],
    '~^users/(\d+)/activate/(.+)$~' => [App\Controllers\UserController::class, 'activate'],
    '~^users/login$~'               => [App\Controllers\UserController::class, 'login'],
    '~^users/logout$~'              => [App\Controllers\UserController::class, 'logout'],
    '~^page/(\d+)$~'                => [App\Controllers\MainController::class, 'main'],
    '~^$~'                          => [App\Controllers\MainController::class, 'main']
];
<?php

namespace App\Controllers;

use App\Models\Users\User;
use App\Services\UserAuthService;
use App\Views\View;
use Exception;

abstract class AbstractController
{
    /** @var View */
    protected $view;

    /** @var User|null */
    protected $user;

    /**
     * AbstractController constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->user = UserAuthService::getUserByToken();
        $this->view = new View(__DIR__ . '/../../templates');
        $this->view->setVar('route', $_SERVER['REQUEST_URI']);
        $this->view->setVar('user', $this->user);
    }
}
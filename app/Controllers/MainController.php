<?php

namespace App\Controllers;

use App\Models\Comments\Comment;
use Exception;
use JasonGrimes\Paginator;

class MainController extends AbstractController
{
    /**
     * Количество комментариев на странице
     *
     * @var int
     */
    protected $itemsPerPage = 5;

    /**
     * Маска адресов страниц с пагинацией
     *
     * @var string
     */
    protected $urlPattern = '/page/(:num)';

    /**
     * Отображение главной страницы
     *
     * @param int|null $number
     *
     * @throws Exception
     */
    public function main(int $number = null): void
    {
        $sorting = 'date';
        $sortDesc = true; // DESC

        // Разберемся с сортировкой
        if (isset($_GET['sort'])) {
            if (isset($_COOKIE['sorting'])) {
                $sorting = $_GET['sort'];
                if ($_COOKIE['sorting'] == $_GET['sort']) {
                    $sortDesc = !$sortDesc;
                } else {
                    setcookie('sorting', $sorting = (string) ($_GET['sort']));
                }
            } else {
                setcookie('sorting', $sorting = (string) ($_GET['sort']));
            }
        } else {
            if (isset($_COOKIE['sorting'])) {
                $sorting = $_COOKIE['sorting'];
            }
        }

        $sorting = htmlspecialchars($sorting);

        switch ($sorting) {
            case 'name':
                $sortByColumn = 'user_name';
                break;
            case 'email':
                $sortByColumn = 'user_email';
                break;
            default:
                $sortByColumn = 'created_at';
        }

        $currentPage = $number ?? 1;
        $offset = $number ? ($number * $this->itemsPerPage - $this->itemsPerPage) : 0;

        // Админу отображаем все комментарии, остальным — только принятые
        $condition = $this->user && $this->user->isAdmin() ? '' : 'status = 1';

        // Получаем массив комментариев и общее количество комментариев
        list($comments, $totalItems) = Comment::paginate($this->itemsPerPage, $offset, $sortByColumn, $sortDesc, $condition);

        // Настраиваем пагинацию
        $paginator = new Paginator((int) $totalItems, $this->itemsPerPage, $currentPage, $this->urlPattern);

        $this->view->render('main/index', compact('comments', 'paginator'));
    }
}

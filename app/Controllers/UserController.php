<?php

namespace App\Controllers;

use App\Exceptions\ActivateException;
use App\Exceptions\InvalidArgumentException;
use App\Models\Users\User;
use App\Models\Users\UserActivationService;
use App\Services\EmailSender;
use App\Services\UserAuthService;
use Exception;

class UserController extends AbstractController
{
    /**
     * Регистрация
     *
     * @throws Exception
     */
    public function register(): void
    {
        if (!empty($_POST)) {
            try {
                $user = User::register($_POST);
            } catch (InvalidArgumentException $e) {
                $this->view->render('users/register', ['error' => $e->getMessage()]);
                return;
            }

            if ($user instanceof User) {
                $code = UserActivationService::createActivationCode($user);

                EmailSender::send($user, 'Активация', 'userActivation', [
                    'userId' => $user->getId(),
                    'code'   => $code
                ]);

                $this->view->render('users/registerSuccessful');
                return;
            }
        }

        $this->view->render('users/register');
    }

    /**
     * Активация пользователя
     *
     * @param int    $userId
     * @param string $activationCode
     *
     * @throws Exception
     */
    public function activate(int $userId, string $activationCode): void
    {
        try {
            User::activate($userId, $activationCode);
            UserActivationService::deleteActivationCode($userId);
            $this->view->render('users/activationSuccess');
        } catch (ActivateException $e) {
            $this->view->render('errors/activationError', ['error' => $e->getMessage()]);
            return;
        }
    }

    /**
     * Авторизация
     *
     * @throws Exception
     */
    public function login(): void
    {
        if (!empty($_POST)) {
            try {
                $user = User::login($_POST);
                UserAuthService::createToken($user);
                header('Location: /');
                exit;
            } catch (InvalidArgumentException $e) {
                $this->view->render('users/login', ['error' => $e->getMessage()], 404);
                return;
            }
        }

        $this->view->render('users/login');
    }

    /**
     * Выход
     *
     * @throws Exception
     */
    public function logout(): void
    {
        UserAuthService::removeToken(UserAuthService::getUserByToken());
        header('Location: /');
    }
}
<?php

namespace App\Controllers;

use App\Exceptions\ForbiddenException;
use App\Exceptions\InvalidArgumentException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UnauthorizedException;
use App\Models\Comments\Comment;
use Exception;

class CommentController extends AbstractController
{
    /**
     * @throws Exception
     */
    public function add()
    {
        if (!empty($_POST)) {
            try {
                $comment = Comment::createFromArray($_POST, $this->user);

                if ($comment) {
                    echo json_encode([
                        'status' => 'ok',
                        'admin'  => $this->user ? $this->user->isAdmin() : false
                    ]);
                }
            } catch (InvalidArgumentException $e) {
                echo json_encode(['error' => $e->getMessage()]);
            }
        }
    }

    /**
     * @param int $id
     *
     * @throws Exception
     */
    public function edit(int $id): void
    {
        if ($this->user === null) {
            throw new UnauthorizedException('Авторизуйтесь, пожалуйста!');
        }

        if (!$this->user->isAdmin()) {
            throw new ForbiddenException('Редактировать комментарии могут только администраторы!');
        }

        $comment = Comment::getById($id);

        if ($comment === null) {
            throw new NotFoundException('Комментарий не найден!');
        }

        if (!empty($_POST)) {
            try {
                $comment->updateFromArray($_POST);
            } catch (InvalidArgumentException $e) {
                $this->view->render('comments/edit', ['error' => $e->getMessage()]);
                return;
            }

            header('Location: /', true, 302);
            exit;
        }

        $title = 'Редактирование комментария';

        $this->view->render('comments/edit', compact('comment', 'title'));
    }

    /**
     * @param int $id
     *
     * @throws Exception
     */
    public function toggle(int $id)
    {
        $comment = Comment::getById($id);

        if ($comment) {
            $comment->toggleStatus();

            header('Location: /', true, 302);
            exit;
        }
    }

    /**
     * @param int $id
     *
     * @throws Exception
     */
    public function delete(int $id): void
    {
        if ($this->user === null) {
            throw new UnauthorizedException('Авторизуйтесь, пожалуйста!');
        }

        if (!$this->user->isAdmin()) {
            throw new ForbiddenException('Редактировать комментарии могут только администраторы!');
        }

        $comment = Comment::getById($id);

        if ($comment === null) {
            throw new NotFoundException('Комментарий не найден!');
        }

        $comment->delete();

        header('Location: /', true, 302);
        exit;
    }
}
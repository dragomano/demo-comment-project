<?php

namespace App\Models\Comments;

use App\Exceptions\InvalidArgumentException;
use App\Models\ActiveRecordEntity;
use App\Models\Users\User;
use App\Services\Upload;
use Exception;

class Comment extends ActiveRecordEntity
{
    /** @var int */
    protected $id;

    /** @var string */
    protected $userName;

    /** @var string */
    protected $userEmail;

    /** @var int */
    protected $userId;

    /** @var string */
    protected $content;

    /** @var string */
    protected $image;

    /** @var int */
    protected $status;

    /** @var string */
    protected $createdAt;

    /** @var string */
    protected $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getAuthor(): string
    {
        if ($this->userId) {
            if ($user = User::getById($this->userId)) {
                return $user->getNickname();
            }
        }

        return $this->getUserName();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getEmail(): string
    {
        if ($this->userId) {
            if ($user = User::getById($this->userId)) {
                return $user->getEmail();
            }
        }

        return $this->getUserEmail();
    }

    /**
     * @return string|null
     */
    public function getUserName(): ?string
    {
        return $this->userName;
    }

    /**
     * @return string|null
     */
    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @param string $value
     * @return void
     */
    public function setContent($value): void
    {
        $this->content = $value;
    }

    /**
     * @param $value
     */
    public function setImage($value): void
    {
        $this->image = $value;
    }

    /**
     * @param User $author
     * @return void
     */
    public function setAuthor(User $author): void
    {
        $this->userId = $author->getId();
    }

    /**
     * @param $value
     * @return void
     */
    public function setName($value): void
    {
        $this->userName = $value;
    }

    /**
     * @param $value
     * @return void
     */
    public function setEmail($value): void
    {
        $this->userEmail = $value;
    }

    /**
     * @param $value
     * @return void
     */
    public function setStatus($value): void
    {
        $this->status = $value;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function toggleStatus(): void
    {
        $this->status = (int) !$this->status;
        $this->save();
    }

    /**
     *
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = date('Y-m-d H:i:s');
    }

    /**
     * @return string
     */
    protected static function getTableName(): string
    {
        return 'comments';
    }

    /**
     * @param array $fields
     * @param User  $author
     *
     * @return Comment
     * @throws Exception
     */
    public static function createFromArray(array $fields, User $author = null): Comment
    {
        foreach ($fields as $k => $v) {
            $fields[$k] = htmlspecialchars(stripslashes($v));
        }

        if (empty($fields['name']) && empty($author)) {
            throw new InvalidArgumentException('Не указано имя комментатора!');
        }

        if (empty($fields['email']) && empty($author)) {
            throw new InvalidArgumentException('Не указан email комментатора!');
        }

        if (empty($fields['content'])) {
            throw new InvalidArgumentException('Не передан текст комментария!');
        }

        $comment = new Comment();

        if (!empty($fields['name'])) {
            if (!preg_match('/^([а-яё\s]+|[a-z\s]+)$/iu', $fields['name'], $matches, PREG_OFFSET_CAPTURE, 0)) {
                throw new InvalidArgumentException('Имя может состоять только из символов латинского или русского алфавитов!');
            }

            $comment->setName($fields['name']);
        } else {
            $comment->setName($author->getNickname());
        }

        if (!empty($fields['email'])) {
            if (!filter_var($fields['email'], FILTER_VALIDATE_EMAIL)) {
                throw new InvalidArgumentException('Email некорректен!');
            }

            $comment->setEmail($fields['email']);
        } else {
            $comment->setEmail($author->getEmail());
        }

        if ($author) {
            $comment->setAuthor($author);

            if ($author->isAdmin()) {
                $comment->setStatus(1);
            }
        }

        $comment->setContent($fields['content']);

        // Если указано изображение, обработаем и загрузим
        if ($_FILES['image']) {
            $img   = new Upload($_FILES['image']);
            $image = $img->upload();
            $comment->setImage($image);
        }

        $comment->save();

        return $comment;
    }

    /**
     * @param array $fields
     *
     * @return Comment
     * @throws Exception
     */
    public function updateFromArray(array $fields): Comment
    {
        foreach ($fields as $k => $v) {
            $fields[$k] = htmlspecialchars(stripslashes($v));
        }

        if (empty($fields['content'])) {
            throw new InvalidArgumentException('Не передан текст комментария');
        }

        $this->setContent($fields['content']);
        $this->setUpdatedAt();
        $this->save();

        return $this;
    }

    /**
     * @return string
     */
    public function getParsedContent(): string
    {
        $parser = new \Parsedown();
        $parser->setMarkupEscaped(true);

        return $parser->text($this->getContent());
    }
}
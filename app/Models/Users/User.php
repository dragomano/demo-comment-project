<?php

namespace App\Models\Users;

use App\Exceptions\ActivateException;
use App\Exceptions\InvalidArgumentException;
use App\Models\ActiveRecordEntity;
use Exception;

class User extends ActiveRecordEntity
{
    /** @var string */
    protected $nickname;

    /** @var string */
    protected $login;

    /** @var string */
    protected $email;

    /** @var int */
    protected $isConfirmed;

    /** @var string */
    protected $role;

    /** @var string */
    protected $passwordHash;

    /** @var string */
    protected $authToken;

    /** @var string */
    protected $createdAt;

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    protected static function getTableName(): string
    {
        return 'users';
    }

    /**
     * @param array $userData
     *
     * @return User
     * @throws Exception
     */
    public static function register(array $userData): User
    {
        if (empty($userData['nickname'])) {
            throw new InvalidArgumentException('Не передан никнейм');
        }

        if (!preg_match('/[a-zA-Z0-9]+/', $userData['nickname'])) {
            throw new InvalidArgumentException('Никнейм может состоять только из символов латинского алфавита и цифр');
        }

        if (empty($userData['login'])) {
            throw new InvalidArgumentException('Не передан логин');
        }

        if (!preg_match('/[a-zA-Z0-9]+/', $userData['login'])) {
            throw new InvalidArgumentException('Логин может состоять только из символов латинского алфавита и цифр');
        }

        if (empty($userData['email'])) {
            throw new InvalidArgumentException('Не передан email');
        }

        if (!filter_var($userData['email'], FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('Email некорректен');
        }

        if (empty($userData['password'])) {
            throw new InvalidArgumentException('Не передан password');
        }

        if (mb_strlen($userData['password']) < 8) {
            throw new InvalidArgumentException('Пароль должен быть не менее 8 символов');
        }

        if (static::findOneByColumn('login', $userData['login']) !== null) {
            throw new InvalidArgumentException('Пользователь с таким логином уже существует');
        }

        if (static::findOneByColumn('email', $userData['email']) !== null) {
            throw new InvalidArgumentException('Пользователь с таким email уже существует');
        }

        $user = new User();
        $user->nickname = $userData['nickname'];
        $user->login = $userData['login'];
        $user->email = $userData['email'];
        $user->passwordHash = password_hash($userData['password'], PASSWORD_DEFAULT);
        $user->isConfirmed = false;
        $user->role = 'user';
        $user->authToken = sha1(random_bytes(100)) . sha1(random_bytes(100));
        $user->save();

        return $user;
    }

    /**
     *
     * @param int    $userId
     * @param string $activationCode
     *
     * @throws ActivateException
     * @throws Exception
     */
    public static function activate(int $userId, string $activationCode): void
    {
        $user = User::getById($userId);

        if ($user === null) {
            throw new ActivateException('Нет такого пользователя');
        }

        if ($user->isConfirmed()) {
            throw new ActivateException('Пользователь уже активирован');
        }

        if (!UserActivationService::existCode($userId)) {
            throw new ActivateException('Не создан код активации');
        }

        if (!UserActivationService::checkActivationCode($user, $activationCode)) {
            throw new ActivateException('Код активации неверен');
        }

        $user->isConfirmed = true;
        $user->save();
    }

    /**
     * @param array $loginData
     *
     * @return User
     * @throws Exception
     */
    public static function login(array $loginData): User
    {
        if (empty($loginData['login'])) {
            throw new InvalidArgumentException('Не передан логин');
        }

        if (empty($loginData['password'])) {
            throw new InvalidArgumentException('Не передан пароль');
        }

        $user = User::findOneByColumn('login', $loginData['login']);
        if ($user === null) {
            throw new InvalidArgumentException('Нет пользователя с таким логином');
        }

        if (!password_verify($loginData['password'], $user->getPasswordHash())) {
            throw new InvalidArgumentException('Неправильный пароль');
        }

        if (!$user->isConfirmed) {
            throw new InvalidArgumentException('Пользователь не подтверждён');
        }

        $user->refreshAuthToken();
        $user->save();

        return $user;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    /**
     * @throws Exception
     */
    public function refreshAuthToken(): void
    {
        $this->authToken = sha1(random_bytes(100)) . sha1(random_bytes(100));
    }

    /**
     * @return string
     */
    public function getAuthToken(): string
    {
        return $this->authToken;
    }

    /**
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return (bool) $this->isConfirmed;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->getRole() === 'admin';
    }
}
<?php

namespace App\Models\Users;

use App\Services\Db;
use Exception;

class UserActivationService
{
    /**
     * Название текущей таблицы в бд
     */
    private const TABLE_NAME = 'activation_codes';

    /**
     * @param User $user
     *
     * @return string
     * @throws Exception
     */
    public static function createActivationCode(User $user): string
    {
        // Генерируем случайную последовательность символов
        $code = bin2hex(random_bytes(16));

        $db = Db::getInstance();
        $db->query(
            'INSERT INTO ' . self::TABLE_NAME . ' (user_id, code) VALUES (:user_id, :code)',
            [
                'user_id' => $user->getId(),
                'code'    => $code
            ]
        );

        return $code;
    }

    /**
     * @param User   $user
     * @param string $code
     *
     * @return bool
     * @throws Exception
     */
    public static function checkActivationCode(User $user, string $code): bool
    {
        $db = Db::getInstance();
        $result = $db->query(
            'SELECT * FROM ' . self::TABLE_NAME . ' WHERE user_id = :user_id AND code = :code',
            [
                'user_id' => $user->getId(),
                'code'    => $code
            ]
        );

        return !empty($result);
    }

    /**
     * @param int $userId
     *
     * @return bool
     * @throws Exception
     */
    public static function existCode(int $userId): bool
    {
        $db = Db::getInstance();
        $result = $db->query(
            'SELECT * FROM ' . self::TABLE_NAME . ' WHERE user_id = :user_id',
            [
                'user_id' => $userId
            ]
        );

        return !empty($result);
    }

    /**
     * @param int $userId
     *
     * @throws Exception
     */
    public static function deleteActivationCode(int $userId): void
    {
        $db = Db::getInstance();
        $db->query(
            'DELETE FROM ' . self::TABLE_NAME . ' WHERE user_id = :user_id',
            [
                'user_id' => $userId
            ]
        );
    }
}
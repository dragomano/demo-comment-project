<?php

namespace App\Models;

use App\Services\Db;
use Exception;

abstract class ActiveRecordEntity
{
    /** @var int */
    protected $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @param        $value
     */
    public function __set(string $name, $value): void
    {
        $camelCaseName = $this->underscoreToCamelCase($name);
        $this->$camelCaseName = $value;
    }

    /**
     * @param string $source
     *
     * @return string
     */
    private function underscoreToCamelCase(string $source): string
    {
        return lcfirst(str_replace('_', '', ucwords($source, '_')));
    }

    /**
     *
     * @throws Exception
     */
    public function save(): void
    {
        $mappedProperties = $this->mapPropertiesToDbFormat();
        if ($this->id !== null) {
            $this->update($mappedProperties);
        } else {
            $this->insert($mappedProperties);
        }
    }

    /**
     * @param array $mappedProperties
     *
     * @throws Exception
     */
    private function update(array $mappedProperties): void
    {
        $columns2params = [];
        $params2values = [];
        $index = 1;
        foreach ($mappedProperties as $column => $value) {
            $param = ':param' . $index; // :param1
            $columns2params[] = $column . ' = ' . $param; // column1 = :param1
            $params2values[':param' . $index] = $value; // [:param1 => value1]
            $index++;
        }
        $sql = 'UPDATE ' . static::getTableName() . ' SET ' . implode(', ', $columns2params) . ' WHERE id = ' . $this->id;
        $db = Db::getInstance();
        $db->query($sql, $params2values, static::class);
    }

    /**
     * @param array $mappedProperties
     *
     * @throws Exception
     */
    private function insert(array $mappedProperties): void
    {
        $filteredProperties = array_filter($mappedProperties);

        $columns = [];
        $paramsNames = [];
        $params2values = [];
        foreach ($filteredProperties as $columnName => $value) {
            $columns[] = '`' . $columnName. '`';
            $paramName = ':' . $columnName;
            $paramsNames[] = $paramName;
            $params2values[$paramName] = $value;
        }

        $columnsViaSemicolon = implode(', ', $columns);
        $paramsNamesViaSemicolon = implode(', ', $paramsNames);

        $sql = 'INSERT INTO ' . static::getTableName() . ' (' . $columnsViaSemicolon . ') VALUES (' . $paramsNamesViaSemicolon . ');';

        $db = Db::getInstance();
        $db->query($sql, $params2values, static::class);
        $this->id = $db->getLastInsertId();
        $this->refresh();
    }

    /**
     *
     * @throws Exception
     */
    private function refresh(): void
    {
        $objectFromDb = static::getById($this->id);
        $reflector = new \ReflectionObject($objectFromDb);
        $properties = $reflector->getProperties();

        foreach ($properties as $property) {
            $property->setAccessible(true);
            $propertyName = $property->getName();
            $this->$propertyName = $property->getValue($objectFromDb);
        }
    }

    /**
     * @throws Exception
     */
    public function delete(): void
    {
        $db = Db::getInstance();
        $db->query(
            'DELETE FROM `' . static::getTableName() . '` WHERE id = :id',
            [':id' => $this->id]
        );
        $this->id = null;
    }

    /**
     * @return array
     */
    private function mapPropertiesToDbFormat(): array
    {
        $reflector = new \ReflectionObject($this);
        $properties = $reflector->getProperties();

        $mappedProperties = [];
        foreach ($properties as $property) {
            $propertyName = $property->getName();
            $propertyNameAsUnderscore = $this->camelCaseToUnderscore($propertyName);
            $mappedProperties[$propertyNameAsUnderscore] = $this->$propertyName;
        }

        return $mappedProperties;
    }

    /**
     * @param string $source
     *
     * @return string
     */
    private function camelCaseToUnderscore(string $source): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $source));
    }

    /**
     * @param int    $limit
     * @param int    $offset
     * @param string $sortByColumn
     * @param bool   $sortDesc
     * @param string $condition
     *
     * @return static[]
     * @throws Exception
     */
    public static function paginate(int $limit, int $offset = 0, string $sortByColumn = 'id', bool $sortDesc = true, string $condition = ''): array
    {
        $db = Db::getInstance();

        $params = [$limit, $offset];
        $count_params = [];
        if ($condition) {
            $condition = explode('=', $condition);
            $params = [$condition[1], $limit, $offset];
            $count_params = [$condition[1]];
        }

        $totalCount = $db->query(
            'SELECT COUNT(*) AS total FROM `' . static::getTableName() . '`' . ($condition ? (' WHERE ' . $condition[0] . '= ?') : '') . ';',
            $count_params,
            static::class
        )[0]->total;

        return [$db->query(
            'SELECT * FROM `' . static::getTableName() . '`' . ($condition ? ' WHERE ' . $condition[0] . '= ?' : '') . ' ORDER BY ' . $sortByColumn . ' ' . ($sortDesc ? 'DESC' : 'ASC') . ' LIMIT ? OFFSET ?;',
            $params,
            static::class
        ), $totalCount];
    }

    /**
     * @param int $id
     *
     * @return static|null
     * @throws Exception
     */
    public static function getById(int $id): ?self
    {
        $db = Db::getInstance();
        $entities = $db->query(
            'SELECT * FROM `' . static::getTableName() . '` WHERE id=:id;',
            [':id' => $id],
            static::class
        );

        return $entities ? $entities[0] : null;
    }

    /**
     * @param string $sortBy
     *
     * @return static[]
     * @throws Exception
     */
    public static function findAll(string $sortBy = 'DESC'): array
    {
        $db = Db::getInstance();

        return $db->query(
            'SELECT * FROM `' . static::getTableName() . '` ORDER BY ?;',
            [$sortBy],
            static::class);
    }

    /**
     * @param string $columnName
     * @param        $value
     *
     * @return static|null
     * @throws Exception
     */
    public static function findOneByColumn(string $columnName, $value): ?self
    {
        $db = Db::getInstance();
        $result = $db->query(
            'SELECT * FROM `' . static::getTableName() . '` WHERE `' . $columnName . '` = :value LIMIT 1;',
            [':value' => $value],
            static::class
        );

        if ($result === []) {
            return null;
        }

        return $result[0];
    }

    /**
     * @param string $columnName
     * @param string $value
     *
     * @return array|null
     * @throws Exception
     */
    public static function findAllByColumn(string $columnName, string $value): ?array
    {
        $db = Db::getInstance();
        $result = $db->query(
            'SELECT * FROM `' . static::getTableName() . '` WHERE `' . $columnName . '` = :value;',
            [':value' => $value],
            static::class
        );

        if ($result === []) {
            return null;
        }

        return $result;
    }

    /**
     * @return string
     */
    abstract protected static function getTableName(): string;
}
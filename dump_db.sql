-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `task_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `task_db`;

DROP TABLE IF EXISTS `activation_codes`;
CREATE TABLE `activation_codes` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `user_id` int(11) NOT NULL,
                                    `code` varchar(255) NOT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `user_id` (`user_id`),
                                    CONSTRAINT `activation_codes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
                            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                            `user_name` varchar(255) DEFAULT NULL,
                            `user_email` varchar(255) DEFAULT NULL,
                            `user_id` int(11) NOT NULL DEFAULT 0,
                            `content` text NOT NULL,
                            `image` varchar(255) DEFAULT NULL,
                            `status` tinyint(1) unsigned NOT NULL DEFAULT 0,
                            `created_at` datetime NOT NULL DEFAULT current_timestamp(),
                            `updated_at` datetime DEFAULT NULL,
                            PRIMARY KEY (`id`),
                            KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `nickname` varchar(128) NOT NULL,
                         `login` varchar(128) NOT NULL,
                         `email` varchar(255) NOT NULL,
                         `is_confirmed` tinyint(1) NOT NULL DEFAULT 0,
                         `role` enum('admin','user') NOT NULL,
                         `password_hash` varchar(255) NOT NULL,
                         `auth_token` varchar(255) NOT NULL,
                         `created_at` datetime NOT NULL DEFAULT current_timestamp(),
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `email` (`email`),
                         UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `nickname`, `login`, `email`, `is_confirmed`, `role`, `password_hash`, `auth_token`, `created_at`) VALUES
(1,	'Admin',	'admin',	'a@a.com',	1,	'admin',	'$2y$10$0AQbX9NXPjzgrGUdfmnVaOokUNhuKKjid8/OVmt/2jcAwjT32/4S.',	'263b8c168258dc02731e9d2414a957c4cd6ceb6d10acb1e330851dd2321aea66f322119f677125a8',	'2019-07-19 16:36:13'),
(2,	'User',	'user',	'u@u.com',	1,	'user',	'$2y$10$0AQbX9NXPjzgrGUdfmnVaOokUNhuKKjid8/OVmt/2jcAwjT32/4S.',	'360d6d80a4d7c6a098cbc7bd2cbbd22fefefca8a4c12b2fa9a6d3e52918dd57f60ebf0aa59562c2f',	'2019-07-13 20:51:08')

-- 2019-10-26 19:10:49
